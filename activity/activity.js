// $or operator
db.users.find( { $or : [ { firstName: "s" }, { lastName : "t" } ] } );

// $and operator
db.users.find( { $and: [ { department: "HR" }, { age: { $gte : 70} } ] } );

// $and, $regex and $lte operators
db.users.find({ $and: [ { firstName: "e"}], $regex: [{ firstName: "e" }], $lte:[{ age: 30 }]  })